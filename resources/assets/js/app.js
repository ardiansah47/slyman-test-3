require('./bootstrap');
require('./plugin/select2.min');
var swal = require('./plugin/sweetalert2.min');

$(document).ready(function() {
    $('.next').click(function() {
        $('.nav-pills > .active').next('li').find('a').trigger('click');
        $('.nav-pills > li.done').next('li').addClass('done');
        var device = $('#device').val();
        var repair = $('#repair').val();
        $("#device-selected > span").html(device);   
        $("#repair-selected > span").html(repair);             
        // console.log(device);
    });
    $('.prev').click(function() {
        $('.nav-pills > .active').prev('li').find('a').trigger('click');
        $('.nav-pills > .active').next('li').removeClass('done');
    });
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        var step = $(e.target).data('step');
        var percent = (parseInt(step) / 3) * 100;
        $('.progress-bar').css({ width: percent + '%' });
    })
    $('.first').click(function() {
        $('#myWizard a:first').tab('show')
        $('.nav-pills > .active').next('li').removeClass('done');
        $('.nav-pills > li:last-child').removeClass('done');
    })
    $('#repair').on('change', function() {
	    event.preventDefault();
   		$('.next').prop("disabled", false);
	});
     $('.select-custom').select2({
        minimumResultsForSearch: -1,
        width: '100%',
        height: "20px"
     }).on('select2-open', function() {

        // however much room you determine you need to prevent jumping
        var requireHeight = 600;
        var viewportBottom = $(window).scrollTop() + $(window).height();

        // figure out if we need to make changes
        if (viewportBottom < requireHeight) 
        {           
            // determine how much padding we should add (via marginBottom)
            var marginBottom = requireHeight - viewportBottom;

            // adding padding so we can scroll down
            $(".aLwrElmntOrCntntWrppr").css("marginBottom", marginBottom + "px");

            // animate to just above the select2, now with plenty of room below
            $('html, body').animate({
                scrollTop: $(".select-custom").offset().top - 10
            }, 1000);
        }
    });
    $('.finish').click(function(){
        swal({title: "Thank you for ordering", text: "Check your email for more information", type: 
        "success"}).then(function(){ 
           location.reload();
           }
        );
    });
});